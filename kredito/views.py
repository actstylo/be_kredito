from django.conf import settings
from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import LANGUAGE_SESSION_KEY


from kredito.models import AnonymousUser
from kredito.decorators import login_required
from kredito.forms import LoginForm, RegistrationForm


@login_required
def home(request):
    return render(request, 'kredito/home.html')


def login(request):
    if request.session._get_session_key() is None:
        request.session.modified = True
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid() and form.token:
            request.session[settings.SESSION_TOKEN_KEY] = form.token.get(settings.SESSION_TOKEN_KEY)
            request.session[settings.SESSION_TOKEN_TYPE_KEY] = form.token.get(settings.SESSION_TOKEN_TYPE_KEY)
            request.session.modified = True
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
    return render(request, 'kredito/login.html', {'form': form})


def logout(request):
    """
    Remove the authenticated user's ID from the request and flush their session
    data.
    """
    # Dispatch the signal before the user is logged out so the receivers have a
    # chance to find out *who* logged out.
    user = getattr(request, 'user', None)
    if not getattr(user, 'is_authenticated', True):
        user = None

    # remember language choice saved to session
    language = request.session.get(LANGUAGE_SESSION_KEY)

    request.session.flush()

    if language is not None:
        request.session[LANGUAGE_SESSION_KEY] = language

    if hasattr(request, 'user'):
        request.user = AnonymousUser()
    return HttpResponseRedirect(settings.LOGOUT_REDIRECT_URL)


def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid() and form.user:
            messages.info(request, _('Registration successfully completed.'))
            return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, 'kredito/registration.html', {'form': form})
