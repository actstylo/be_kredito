from django.conf import settings


from api_client import Client


client = Client()


def get_user(request):
    """
    Return the user model instance associated with the given request session.
    If no user is retrieved, return an instance of `AnonymousUser`.
    """
    from .models import AnonymousUser, User
    user = None
    try:
        data = {
            settings.SESSION_TOKEN_KEY: request.session[settings.SESSION_TOKEN_KEY],
            settings.SESSION_TOKEN_TYPE_KEY: request.session[settings.SESSION_TOKEN_TYPE_KEY]
        }
    except KeyError:
        pass
    else:
        user_info = client.get_user_info(**data)
        if user_info:
            user = User(**user_info)
    return user or AnonymousUser()


def get_language():
    return client.get_language()
