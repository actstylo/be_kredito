from django import forms
from django.utils.translation import ugettext_lazy as _

from api_client import Client


client = Client()


class LoginForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.token = None

    email = forms.EmailField(
        required=True
    )

    password = forms.CharField(
        widget=forms.PasswordInput,
        required=True
    )

    def clean(self):
        super().clean()
        if self.is_valid():
            is_success, token = client.get_login_jwt(**self.cleaned_data)
            if is_success:
                self.token = token
            else:
                for field, errors in token.get('errors', {}).items():
                    if field in self.cleaned_data:
                        self.add_error(field, errors)
        return self.cleaned_data


class RegistrationForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    code = forms.CharField(
        strip=True,
        min_length=15,
        max_length=50,
        required=True
    )

    email = forms.EmailField(
        required=True,
        max_length=255
    )

    password = forms.CharField(
        widget=forms.PasswordInput,
        required=True,
        min_length=6
    )

    password_confirmation = forms.CharField(
        widget=forms.PasswordInput,
        required=True
    )

    agreement = forms.BooleanField(
        required=True
    )

    def clean(self):
        super().clean()
        if self.cleaned_data.get('password') != self.cleaned_data.get('password_confirmation'):
            self.add_error('password', forms.ValidationError(_("Password does not match."), code='invalid'))
        if self.is_valid():
            is_success, user = client.get_registered(**self.cleaned_data)
            if is_success:
                self.user = user
            else:
                for field, errors in user.get('errors', {}).items():
                    if field in self.cleaned_data:
                        self.add_error(field, errors)
        return self.cleaned_data
