from django.db import models


class User:

    def __init__(self, id, email, email_verified_at, created_at,
                 phone=None, phone_verified_at=None, country=None, locale=None, *args, **kwargs):
        self.id = id
        self.email = email
        self.email_verified_at = email_verified_at
        self.created_at = created_at
        self.phone = phone
        self.phone_verified_at = phone_verified_at
        self.country = country
        self.locale = locale

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    def __eq__(self, other):
        return isinstance(other, self.__class__) and other.id == self.id

    def __hash__(self):
        return hash(self.id)

    def __str__(self):
        return f"{self.email}"


class AnonymousUser:

    id = None

    def __int__(self):
        raise TypeError('Cannot cast AnonymousUser to int. Are you trying to use it in place of User?')

    def __str__(self):
        return 'AnonymousUser'

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __hash__(self):
        return 1  # instances always return the same hash value

    @property
    def is_anonymous(self):
        return True

    @property
    def is_authenticated(self):
        return False
