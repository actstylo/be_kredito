Be Kredito rebiton
==================


## Deployment (Without docker)

### Clone Repo


```bash 
$ git clone git@gitlab.com:actstylo/be_kredito.git
$ cd be_kredito
```

### Create a virtualenv 



```bash
$ virtualenv -p python3.6 env
$ source env/bin/activate
(env)$ pip install -r requirements.txt
```

### Run Server (Django Development Server)

```bash 
(env)$ python manage.py compilemessages 
(env)$ python manage.py migrate 
(env)$ python manage.py runserver
```


## Deployment (docker)


### Clone Repo


```bash 
$ git clone git@gitlab.com:actstylo/be_kredito.git
$ cd be_kredito
```

### Install Docker `Ubuntu 18.04`

```bash
$ sudo apt-get update
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
$ sudo apt install docker-ce
```

### Install docker-compose

```bash
$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ sudo docker-compose up
```


## Visit Website `http://127.0.0.1:8000`

