import json
import requests
from requests import exceptions


class Client(object):

    registration_url = 'https://exercise.api.rebiton.com/auth/register'
    login_url = 'https://exercise.api.rebiton.com/auth/login'
    user_details_url = 'https://exercise.api.rebiton.com/user'
    language_url = 'https://exercise.api.rebiton.com/language'

    def __init__(self, headers=None):
        self.headers = headers or {"content-type": "application/json"}


    def get_registered(self, code, email, password, agreement, *args, **kwargs):
        data = {
            'code': code,
            'email': email,
            'password': password,
            'agreement': agreement
        }
        try:
            response = requests.post(
                url=self.registration_url,
                data=json.dumps(data),
                headers=self.headers
            )
            if not (str(response.status_code) == '200' or str(response.status_code) == '201'):
                return False, response.json()
            return True, response.json()
        except exceptions.RequestException as err:
            return False, None

    def get_login_jwt(self, email, password, *args, **kwargs):
        data = {
            'email': email,
            'password': password
        }
        try:
            response = requests.post(
                url=self.login_url,
                data=json.dumps(data),
                headers=self.headers
            )
            if str(response.status_code) != '200':
                return False, response.json()
            return True, response.json().get('data')
        except exceptions.RequestException as err:
            return False, None

    def get_user_info(self, token, token_type):
        headers = {'Authorization': f'{token_type} {token}'}
        headers.update(self.headers)
        try:
            response = requests.get(url=self.user_details_url, headers=headers)
            if str(response.status_code) != '200':
                return None
            return response.json().get('data')
        except exceptions.RequestException as err:
            return None

    def get_language(self):
        try:
            response = requests.get(url=self.language_url)
            language_details = response.json()
            return language_details.get('data', {}).get('language')
        except exceptions.RequestException as err:
            return 'en'



