FROM python:3.6.9

ENV PYTHONUNBUFFERED 1
WORKDIR /code

# Run this first so the first image layer is cached
ADD requirements.txt /code/
RUN apt-get -y update
RUN apt-get install -y gettext
RUN pip install -r requirements.txt

ADD ./ /code/

RUN echo "test"
